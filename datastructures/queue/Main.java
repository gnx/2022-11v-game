package datastructures.queue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String input;
		Queue<String> west = new LinkedList<>();
		Queue<String> north = new LinkedList<>();
		Queue<String> south = new LinkedList<>();
		Queue<String> east = new LinkedList<>();
		Queue<String> current = null;
		Queue<String> combined = new LinkedList<>();
		do {
			input = scan.nextLine();
			if (input.equals("-1")) {
				current = west;
			} else if (input.equals("-2")) {
				current = south;
			} else if (input.equals("-3")) {
				current = north;
			} else if (input.equals("-4")) {
				current = east;
			} else if (!input.equals("0")) {
				current.add(input);
			}
		} while(!input.equals("0"));
		scan.close();
		
		while(!west.isEmpty() || !north.isEmpty() || !south.isEmpty() || !east.isEmpty()) {
			if (!west.isEmpty()) {
				String airplaneToLand = west.poll();
				combined.add(airplaneToLand);
			}
			if (!north.isEmpty()) {
				String airplaneToLand = north.poll();
				combined.add(airplaneToLand);
			}
			if (!south.isEmpty()) {
				String airplaneToLand = south.poll();
				combined.add(airplaneToLand);
			}
			if (!east.isEmpty()) {
				String airplaneToLand = east.poll();
				combined.add(airplaneToLand);
			}
		}
		
		int n = combined.size();
		for (int i = 0; i <  - 1; i++) {
			System.out.print(combined.poll() + " ");
		}
		System.out.print(combined.poll());
	}
}
