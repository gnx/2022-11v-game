package zoo;

public abstract class Animal extends Object {
	String ime;
	double teglo;
	
	void printInfo() {
		System.out.println(ime + " " + teglo);
	}
	
	void printInfo(String pozdrav) {
		printInfo();
		System.out.println(pozdrav);
	}
	
	void printInfo(String pozdrav, String ime) {
		System.out.println(ime + " " + teglo);
		System.out.println(pozdrav);
	}
	
	public String toString() {
		return "Казвам се " + ime + ".\nТежа " + teglo + " килограма.";
	}
}
