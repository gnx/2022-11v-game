package game;

import java.io.Serializable;

public class Hero implements Serializable {
	private static final long serialVersionUID = 1L;
	public int posx;
	public int posy;
	
	public Hero(int x, int y) {
		posx = x;
		posy = y;
	}
}
