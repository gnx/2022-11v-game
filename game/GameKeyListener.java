package game;


import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener implements KeyListener {
	GameMapPanel panel;
	
	GameKeyListener(GameMapPanel p) {
		panel = p;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		char c = e.getKeyChar();
		switch(c) {
			case 'w': panel.karta.hero.posy--; break;
			case 's': panel.karta.hero.posy++; break;
			case 'a': panel.karta.hero.posx--; break;
			case 'd': panel.karta.hero.posx++; break;
		}
		panel.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int c = e.getKeyCode();
		switch(c) {
			case 38: panel.karta.hero.posy--; break;
			case 40: panel.karta.hero.posy++; break;
			case 37: panel.karta.hero.posx--; break;
			case 39: panel.karta.hero.posx++; break;
		}
		panel.repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}
}
